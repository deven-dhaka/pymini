import numpy as np

class linearRegression(object):
    def __init__(self, x, y):
        self.description = "Linear Regression class"
        #
        #

    def train(self):
        # code for training the coefficients
        pass

    def score(self):
        # should result in the error term
        pass

    def predict(self, x):
        # predict values of y for given x
        pass

if __name__ == "__main__": #understand why this is here
    x = np.array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
    y = np.array([1, 3, 2, 5, 7, 8, 8, 9, 10, 12])
    model = linearRegression(x, y)
    model.train()
    model.score()
    x_ = np.array([10, 11, 12, 13, 14, 15, 16])
    model.predict(x_)
